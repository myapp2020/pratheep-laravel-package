<?php

namespace Pratheep\LaravelPackage;

use Illuminate\Support\ServiceProvider;


class PratheepServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('calculato', function ($app) {
      return new Pratheep();
    });
  }

  public function boot()
  {
    //
  }
}
