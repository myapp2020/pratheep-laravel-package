<?php

namespace Pratheep\LaravelPackage;

use Illuminate\Support\ServiceProvider;

class PratheepServiceProvider extends ServiceProvider
{
  public function register()
  {
    //
  }

  public function boot()
  {
    //
  }
}
