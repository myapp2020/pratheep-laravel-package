<?php

namespace Pratheep\LaravelPackage\Facades;

use Illuminate\Support\Facades\Facade;

class Pratheep extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'calculato';
    }
}
